package com.logiqon.fileexplore

import android.app.Application
import android.content.Context
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.util.Util
import com.google.android.gms.ads.MobileAds

class EpicFileExplorerApplication: Application() {

    var appContext: Context? = null
    protected var userAgent: String? = null

    override fun onCreate() {
        super.onCreate()

        appContext = this
        userAgent = Util.getUserAgent(this, "ExoPlayerDemo")

        //Initialise Google AdMob
        MobileAds.initialize(this) {}
    }

    /**
     * Returns a [DataSource.Factory].
     */
    fun buildDataSourceFactory(): DataSource.Factory? {
        return DefaultDataSource.Factory(this, buildHttpDataSourceFactory()!!)
    }

    /**
     * Returns a [HttpDataSource.Factory].
     */
    fun buildHttpDataSourceFactory(): HttpDataSource.Factory? {
        return DefaultHttpDataSource.Factory().setUserAgent(userAgent)
    }
}