package com.logiqon.fileexplore.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.logiqon.fileexplore.R
import java.io.File

class ImagesDisplay : Fragment() {
    private var allpics: ArrayList<File>? = null
    private var currentPosition = 0
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigate(R.id.action_images_Display_to_imagesFragment)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        return inflater.inflate(R.layout.picture_display, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val picture_pager = view.findViewById<ViewPager>(R.id.picture_pager)
        picture_pager.offscreenPageLimit = 3
        // picture_pager.setPageTransformer(true, ZoomOutPageTransformer())
        val adapter = picturePager()
        picture_pager.adapter = adapter
        picture_pager.setCurrentItem(currentPosition, true)
    }

    fun setAllpics(pics: ArrayList<File>?, currentPosition: Int) {
        allpics = pics
        this.currentPosition = currentPosition
    }

    private inner class picturePager : PagerAdapter() {
        @SuppressLint("InflateParams")
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val inflater =
                container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val pictureView: View = inflater.inflate(R.layout.picture_pager_item, null)
            val picturezone = pictureView.findViewById<ImageView>(R.id.picture_zone)
            Glide.with(activity!!)
                .load(Uri.parse(allpics!![position].absolutePath))
                .apply(RequestOptions().fitCenter())
                .into(picturezone)
            (container as ViewPager).addView(pictureView)
            return pictureView
        }

        override fun getCount(): Int {
            return allpics!!.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as View
        }

        override fun destroyItem(containerCollection: ViewGroup, position: Int, view: Any) {
            (containerCollection as ViewPager).removeView(view as View)

        }
    }

}
