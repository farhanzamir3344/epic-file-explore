package com.logiqon.fileexplore.fragment

import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.net.toFile
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.logiqon.fileexplore.Adapters.VideosAdapter
import com.logiqon.fileexplore.Constant
import com.logiqon.fileexplore.Model.VideoDTO
import com.logiqon.fileexplore.PlayerActivity
import com.logiqon.fileexplore.R
import com.logiqon.fileexplore.utils.FileUtils


class VideosFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_videos, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val noRecordView = view.findViewById<LinearLayout>(R.id.llNoRecord)
        val rvVideos: RecyclerView = view.findViewById(R.id.videosrecyclerview)

        rvVideos.setHasFixedSize(true)
        rvVideos.layoutManager = GridLayoutManager(requireContext(), 3)

        val allFilesResult = FileUtils.getAllVideosFromExternalStorage(requireActivity())

        if (allFilesResult.size > 0) {

            val videoAdapter = VideosAdapter(requireContext(), allFilesResult)
            rvVideos.adapter = videoAdapter
            noRecordView!!.visibility = View.GONE
            videoAdapter.setOnItemClickListener { selectedFile ->

                val intent = Intent(requireActivity(), PlayerActivity::class.java)
                intent.putExtra(Constant.CONTENT_URL, selectedFile.filePath)
                intent.putExtra(Constant.CONTENT_NAME, selectedFile.name)
//                intent.putExtra(Constant.CONTENT_IMG_URL, selectedFile.thumbnail)
                startActivity(intent)

            }
        } else {
            //Show No Record
            noRecordView.visibility = View.VISIBLE
        }
    }




}
