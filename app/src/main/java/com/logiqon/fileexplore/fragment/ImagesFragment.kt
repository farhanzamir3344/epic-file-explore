package com.logiqon.fileexplore.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.*
import android.webkit.MimeTypeMap
import android.widget.LinearLayout
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.logiqon.fileexplore.Adapters.ImagesAdapter
import com.logiqon.fileexplore.R
import com.logiqon.fileexplore.utils.FileUtils
import java.io.File


class
ImagesFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_images, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val noRecordView = view.findViewById<LinearLayout>(R.id.llNoRecord)
        val rvImages: RecyclerView? = view.findViewById(R.id.picture_recycler)
        rvImages!!.setHasFixedSize(true)
        rvImages.layoutManager = GridLayoutManager(requireContext(), 4)
        val allFilesResult = FileUtils.getAllImagesFromExternalStorage(requireActivity())


        if (allFilesResult.size > 0) {

            val imageAdapter = ImagesAdapter(requireContext(), allFilesResult)
            rvImages.adapter = imageAdapter
            noRecordView!!.visibility = View.GONE
            imageAdapter.setOnItemClickListener { selectedFile ->
                //Handle selected File
                // val item = allFilesResult[id]
                try {
                    val audioFileIntent = Intent(Intent.ACTION_VIEW)
                    val file = File(selectedFile.filePath)
                    val extension =
                        MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString())
                    val mimetype =
                        MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)

                    val mediaURI = FileProvider.getUriForFile(requireContext(),
                        "com.logiqon.epic.fileexplorer.provider",
                        file)

                    audioFileIntent.setDataAndType(mediaURI, mimetype)
                    audioFileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivity(audioFileIntent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        } else {
            //Show No Record
            noRecordView.visibility = View.VISIBLE
        }
    }

}
