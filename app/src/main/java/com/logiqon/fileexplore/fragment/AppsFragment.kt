package com.logiqon.fileexplore.fragment

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.logiqon.fileexplore.Adapters.AppsAdapter
import com.logiqon.fileexplore.BuildConfig
import com.logiqon.fileexplore.R
import java.io.File

class AppsFragment : Fragment() {
   private var apkFilesResult = ArrayList<File>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_apps, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val noRecordView = view.findViewById<LinearLayout>(R.id.llNoRecord)
        val rvAppa: RecyclerView = view.findViewById(R.id.appssrecyclerview)
        rvAppa.setHasFixedSize(true)
        rvAppa.layoutManager = GridLayoutManager(requireContext(), 1)

        apkFilesResult = getApkFilesList(Environment.getExternalStorageDirectory())

        if (apkFilesResult.isNotEmpty()) {
            val apkadapter = AppsAdapter(requireContext(), apkFilesResult)
            rvAppa.adapter = apkadapter
            noRecordView!!.visibility = GONE

            apkadapter.setOnItemClickListener { selectedFile ->
                //Handle selected File
                try {

                    val packageInfo: PackageInfo? = requireContext().getPackageManager()
                        .getPackageArchiveInfo(selectedFile.path, PackageManager.GET_ACTIVITIES)
                    if (packageInfo != null) {
                        val appInfo = packageInfo.applicationInfo
                        if (Build.VERSION.SDK_INT >= 8) {
                            appInfo.sourceDir = selectedFile.path
                            appInfo.publicSourceDir = selectedFile.path
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            val contentUri = FileProvider.getUriForFile(
                                requireContext(),
                                BuildConfig.APPLICATION_ID + ".provider",
                                selectedFile
                            )
                            val install = Intent(Intent.ACTION_VIEW)
                            install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                            install.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            install.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
                            install.data = contentUri
                            requireContext().startActivity(install)
                            // finish()
                        } else {
                            val install = Intent(Intent.ACTION_VIEW)
                            install.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            install.setDataAndType(
                                Uri.parse(selectedFile.path+ selectedFile.name),
                                "application/${appInfo.packageName}"
                            )
                            requireContext().startActivity(install)
                            // finish()
                        }
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        } else {
            //Show No Record
            val myToast = Toast.makeText(requireContext(),"No Data Found",Toast.LENGTH_SHORT)
            myToast.setGravity(Gravity.CENTER,300,300)
            myToast.show()
            noRecordView!!.visibility = VISIBLE
        }
    }


    private fun getApkFilesList(dir: File): ArrayList<File> {
        val apkDirFilesList = dir.listFiles()
        if (apkDirFilesList != null) {
            for (i in apkDirFilesList.indices) {
                if (apkDirFilesList[i].isDirectory) {
                    getApkFilesList(apkDirFilesList[i])
                } else {
                    if (apkDirFilesList[i].name.endsWith(".apk")
                    ) {
                        apkFilesResult.add(apkDirFilesList[i])
                    }
                }
            }
        }
        return apkFilesResult
    }

}
