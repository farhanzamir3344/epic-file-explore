package com.logiqon.fileexplore.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.*
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.logiqon.fileexplore.Adapters.AllFilesAdapter
import com.logiqon.fileexplore.R
import java.io.File


class
AllFilesFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_allfiles, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val noRecord = view.findViewById<LinearLayout>(R.id.llNoRecord)
        val rvAllFiles: RecyclerView = view.findViewById(R.id.allfilessrecyclerview)

        rvAllFiles.setHasFixedSize(true)
        rvAllFiles.layoutManager = GridLayoutManager(requireContext(), 1)
        val allFilesResult = getAllFilesList(Environment.getExternalStorageDirectory())


        if (allFilesResult.size > 0) {
            val docadapter = AllFilesAdapter(requireContext(), allFilesResult)
            rvAllFiles.adapter = docadapter
            noRecord!!.visibility = View.GONE
            docadapter.setOnItemClickListener { selectedFile ->
                //Handle selected File
                // val item = allFilesResult[id]
                try {
                    val audioFileIntent = Intent(Intent.ACTION_VIEW)
                    val file = File(selectedFile.path)
                    val extension =
                        MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString())
                    val mimetype =
                        MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)

                    val mediaURI = FileProvider.getUriForFile(requireContext(),
                        "com.logiqon.epic.fileexplorer.provider",
                        file)

                    audioFileIntent.setDataAndType(mediaURI, mimetype)
                    audioFileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivity(audioFileIntent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        } else {
            noRecord!!.visibility = View.VISIBLE
        }
    }

    val storageDocs: ArrayList<File> = ArrayList()
    private fun getAllFilesList(dir: File?): ArrayList<File> {
        val allDirFilesList = dir!!.listFiles()
        if (allDirFilesList != null) {
            for (i in allDirFilesList.indices) {
                if (allDirFilesList[i].isDirectory) {
                    getAllFilesList(allDirFilesList[i])
                } else {
                    if (allDirFilesList[i].name.endsWith(".apk")
                        || allDirFilesList[i].name.endsWith(".pdf")
                        || allDirFilesList[i].name.endsWith(".vcf")
                        || allDirFilesList[i].name.endsWith(".png")
                        || allDirFilesList[i].name.endsWith(".jpg")
                        || allDirFilesList[i].name.endsWith(".xlx")
                        || allDirFilesList[i].name.endsWith(".doc")
                        || allDirFilesList[i].name.endsWith(".docx")
                        || allDirFilesList[i].name.endsWith(".jpeg")
                        || allDirFilesList[i].name.endsWith(".mp4")
                        || allDirFilesList[i].name.endsWith(".mov")
                        || allDirFilesList[i].name.endsWith(".wmv")
                        || allDirFilesList[i].name.endsWith(".avi")
                        || allDirFilesList[i].name.endsWith(".avchd")
                        || allDirFilesList[i].name.endsWith(".mkv")
                        || allDirFilesList[i].name.endsWith(".webm")
                        || allDirFilesList[i].name.endsWith(".flv")
                        || allDirFilesList[i].name.endsWith(".f4v")
                        || allDirFilesList[i].name.endsWith(".swf")
                        || allDirFilesList[i].name.endsWith(".mpeg-2")
                    ) {
                        storageDocs.add(allDirFilesList[i])
                    }
                }
            }
        }
        return storageDocs
    }
}
