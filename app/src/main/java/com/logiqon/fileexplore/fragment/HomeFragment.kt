package com.logiqon.fileexplore.fragment

import android.Manifest
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Environment
import android.os.StatFs
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.logiqon.fileexplore.Adapters.FolderListAdapter
import com.logiqon.fileexplore.Model.FolderDTO
import com.logiqon.fileexplore.R
import java.io.File
import kotlin.collections.ArrayList

class
HomeFragment : Fragment(), FolderListAdapter.onItemClickListener {
    var mFolderListAdapter: FolderListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)

    }

    @SuppressLint("ObjectAnimatorBinding", "UseCompatLoadingForDrawables", "SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val noRecordView = view.findViewById<LinearLayout>(R.id.sdcardNo)
        // TextView to show information
        val mTextView = view.findViewById<TextView>(R.id.text_view)
        val mTextView1 = view.findViewById<TextView>(R.id.text_view1)
        val progressbar = view.findViewById<ProgressBar>(R.id.progressbar)
        val progressbar1 = view.findViewById<ProgressBar>(R.id.progressbar1)
        // val drawable = resources.getDrawable(R.drawable.circularprogressbar)


        // Fetching internal memory information
        val iPath: File = Environment.getDataDirectory()
        val iStat = StatFs(iPath.path)
        val iBlockSize = iStat.blockSizeLong
        val iAvailableBlocks = iStat.availableBlocksLong
        val iTotalBlocks = iStat.blockCountLong
        val iAvailableSpace = formatSize(iAvailableBlocks * iBlockSize)
        val iTotalSpace = formatSize(iTotalBlocks * iBlockSize)

        mTextView.text = "$iAvailableSpace\bGB  of  $iTotalSpace\bGB Total "

        try {
            val parsedInt = iTotalSpace.toInt()
            val parsedInt1 = iAvailableSpace.toInt()
            val fgh = parsedInt - parsedInt1
            progressbar.max = parsedInt
            // progressbar.progressDrawable = drawable

            ObjectAnimator.ofInt(progressbar, "progress", fgh)
                .start()
        } catch (ex: NumberFormatException) {
            println("invalid")
        }

        // Fetching external memory information
        val isSDPresent = Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
        val isSDSupportedDevice = Environment.isExternalStorageRemovable()
        val ipath1: File = Environment.getExternalStorageDirectory()
        val iStat1 = StatFs(ipath1.path)
        val iBlockSize1 = iStat1.blockSizeLong
        val iAvailableBlocks1 = iStat1.availableBlocksLong
        val iTotalBlocks1 = iStat1.blockCountLong
        val iAvailableSpace1 = formatSize(iAvailableBlocks1 * iBlockSize1)
        val iTotalSpace1 = formatSize(iTotalBlocks1 * iBlockSize1)
        if (isSDSupportedDevice && isSDPresent) {
            if (iTotalSpace1.isEmpty()) {
                noRecordView!!.visibility = View.GONE
            }else{
                noRecordView.visibility = View.VISIBLE
                mTextView1.text = "$iAvailableSpace1\bGB  of  $iTotalSpace1\bGB Total "

                try {
                    val parsedIntcard = iTotalSpace1.toInt()
                    val parsedIntcard1 = iAvailableSpace1.toInt()
                    val fgh1 = parsedIntcard - parsedIntcard1
                    progressbar1.max = parsedIntcard
                    // progressbar1.progressDrawable = drawable
                    ObjectAnimator.ofInt(progressbar1, "progress", fgh1)
                        .start()
                } catch (ex: NumberFormatException) {
                    println("invalid")
                }
            }
        }

        val recyclerview = view.findViewById<RecyclerView>(R.id.recyclerview)
        recyclerview.layoutManager = GridLayoutManager(activity, 3)
        val data = ArrayList<FolderDTO>()

        data.add(FolderDTO(R.drawable.music, "Music"))
        data.add(FolderDTO(R.drawable.videos, "Videos"))
        data.add(FolderDTO(R.drawable.images, "Images"))
        data.add(FolderDTO(R.drawable.apps, "APK"))
        data.add(FolderDTO(R.drawable.documents, "Documents"))
        data.add(FolderDTO(R.drawable.zip, "Zip file"))
        data.add(FolderDTO(R.drawable.favourites, "My Contacts"))
        data.add(FolderDTO(R.drawable.files, "All files"))

        mFolderListAdapter = FolderListAdapter(data, this)
        recyclerview.adapter = mFolderListAdapter

        requestStoragePermission()
        setHasOptionsMenu(true)

    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateOptionsMenu(menu: Menu, menuinflater: MenuInflater) {
        menuinflater.inflate(R.menu.nav_menu, menu)
        val searchItem = menu.findItem(R.id.action_search)

        if (searchItem != null) {
            //search on recyclerview here
            val searchView = searchItem.actionView as SearchView

            //on text change callback will help filter the list items
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(query: String?): Boolean {
                    if (query!!.length >= 3) {
                        Log.d("onQueryTextChange", "newText: " + query)
                        mFolderListAdapter!!.filter.filter(query)
                        return true
                    } else {
                        mFolderListAdapter!!.filter.filter("")
                        return true
                    }

                }
            })
            return super.onCreateOptionsMenu(menu, menuinflater)

        }

    }


    private fun requestStoragePermission() {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    // check if all permissions are granted
                    if (report.areAllPermissionsGranted()) {
                        Toast.makeText(
                            activity,
                            "All permissions are granted!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    // check for permanent denial of any permission
                    if (report.isAnyPermissionPermanentlyDenied) {
                        // show alert dialog navigating to Settings

                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken,
                ) {
                    token.continuePermissionRequest()
                }
            }).withErrorListener {
                Toast.makeText(
                    activity,
                    "Error occurred! ",
                    Toast.LENGTH_SHORT
                ).show()
            }
            .onSameThread()
            .check()
    }

    override fun onItemClick(position: Int) {
        if (position == 0)
            findNavController().navigate(R.id.action_homepage_to_musicFragment)
        if (position == 1)
            findNavController().navigate(R.id.action_homepage_to_videosFragment)
        if (position == 2)
            findNavController().navigate(R.id.action_homepage_to_imagesFragment)
        if (position == 3)
            findNavController().navigate(R.id.action_homepage_to_appsFragment)
        if (position == 4)
            findNavController().navigate(R.id.action_homepage_to_documentsFragment)
        if (position == 5)
            findNavController().navigate(R.id.action_homepage_to_zipFragment)
        if (position == 6)
            findNavController().navigate(R.id.action_homepage_to_myContactsFragment)
        if (position == 7)
            findNavController().navigate(R.id.action_homepage_to_allFilesFragment)

//        Toast.makeText(requireActivity(), "$position", Toast.LENGTH_SHORT).show()

    }

    private fun formatSize(size: Long): String {
        var size = size
        val suffix: String? = null
        if (size >= 1024) {
            // suffix = "KB"
            size /= 1024
            if (size >= 1024) {
                // suffix = "MB"
                size /= 1024
                if (size >= 1024) {
                    // suffix = "GB"
                    size /= 1024
                }
            }
        }
        val resultBuffer = StringBuilder(java.lang.Long.toString(size))
        // var commaOffset = resultBuffer.length - 3
        // while (commaOffset > 0) {
        //    resultBuffer.insert(commaOffset, ',')
        //    commaOffset -= 3
        // }
        if (suffix != null) resultBuffer.append(suffix)
        return resultBuffer.toString()
    }
}
