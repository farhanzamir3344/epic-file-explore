package com.logiqon.fileexplore.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.*
import android.webkit.MimeTypeMap
import android.widget.LinearLayout
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.logiqon.fileexplore.Adapters.DocumentsAdapter
import com.logiqon.fileexplore.R
import java.io.File
import kotlin.collections.ArrayList

class DocumentsFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_documents, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val rvDocuments: RecyclerView = view.findViewById(R.id.rvDocuments)
        val noRecordView = view.findViewById<LinearLayout>(R.id.llNoRecord)

        rvDocuments.setHasFixedSize(true)
        rvDocuments.layoutManager = GridLayoutManager(requireContext(), 1)
        //Prepare documents files list
        val documentsList = getDocumentsFilesList(Environment.getExternalStorageDirectory())


        if (documentsList.size > 0) {
            noRecordView!!.visibility = View.GONE

            val docAdapter = DocumentsAdapter(requireContext(), documentsList)
            rvDocuments.adapter = docAdapter


            docAdapter.setOnItemClickListener { selectedFile ->
                //Handle selected File
                try {
                    val audioFileIntent = Intent(Intent.ACTION_VIEW)
                    val file = File(selectedFile.path)
                    val extension =
                        MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString())
                    val mimetype =
                        MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)

                    val mediaURI = FileProvider.getUriForFile(requireContext(),
                        "com.logiqon.epic.fileexplorer.provider",
                        file)

                    audioFileIntent.setDataAndType(mediaURI, mimetype)
                    audioFileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivity(audioFileIntent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } else {
            //Show No Record
            noRecordView!!.visibility = View.VISIBLE
        }
    }

    val storageDocs: ArrayList<File> = ArrayList()
    private fun getDocumentsFilesList(dir: File): ArrayList<File> {

        val documentsDirFilesList = dir.listFiles()
        if (documentsDirFilesList != null) {
            for (i in documentsDirFilesList.indices) {
                if (documentsDirFilesList[i].isDirectory) {
                    getDocumentsFilesList(documentsDirFilesList[i])
                } else {
                    if (documentsDirFilesList[i].name.endsWith(".docx")
                        || documentsDirFilesList[i].name.endsWith(".pdf")
                    ) {
                        storageDocs.add(documentsDirFilesList[i])
                    }
                }
            }
        }
        return storageDocs
    }
}
