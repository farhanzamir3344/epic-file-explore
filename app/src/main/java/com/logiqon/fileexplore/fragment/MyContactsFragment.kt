package com.logiqon.fileexplore.fragment

import android.os.Bundle
import android.os.Environment
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.logiqon.fileexplore.Adapters.ContactsAdapter
import com.logiqon.fileexplore.R
import java.io.File

class MyContactsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contacts, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val noRecord = view.findViewById<LinearLayout>(R.id.llNoRecord)
        val rvContact: RecyclerView = view.findViewById(R.id.contactssrecyclerview)
        rvContact.setHasFixedSize(true)
        rvContact.layoutManager = GridLayoutManager(requireContext(), 1)
        val apkFilesResult = getVcfFilesList(Environment.getExternalStorageDirectory())


        if (apkFilesResult.size > 0) {

            val vcfadapter = ContactsAdapter(requireContext(), apkFilesResult)
            rvContact.adapter = vcfadapter
            noRecord!!.visibility = View.GONE
            vcfadapter.setOnItemClickListener {
                //Handle selected File
            }
        } else {
            //Show No Record
            noRecord!!.visibility = View.VISIBLE
        }
    }

    val storageDocs: ArrayList<File> = ArrayList()
    fun getVcfFilesList(dir: File): ArrayList<File> {
        val vcfDirFilesList = dir.listFiles()
        if (vcfDirFilesList != null) {
            for (i in vcfDirFilesList.indices) {
                if (vcfDirFilesList[i].isDirectory) {
                    getVcfFilesList(vcfDirFilesList[i])
                } else {
                    if (vcfDirFilesList[i].name.endsWith(".vcf")
                    ) {
                        storageDocs.add(vcfDirFilesList[i])
                    }
                }
            }
        }
        return storageDocs
    }
}
