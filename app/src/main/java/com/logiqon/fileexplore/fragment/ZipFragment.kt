package com.logiqon.fileexplore.fragment

import android.os.Bundle
import android.os.Environment
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.logiqon.fileexplore.Adapters.ZipAdapter
import com.logiqon.fileexplore.R
import java.io.*
import kotlin.collections.ArrayList

class ZipFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_zip, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val noRecord = view.findViewById<LinearLayout>(R.id.llNoRecord)
        val rvZip: RecyclerView? = view.findViewById(R.id.zipsrecyclerview)
        rvZip!!.setHasFixedSize(true)
        rvZip.layoutManager = GridLayoutManager(requireContext(), 2)

        //Prepare zip files list
        val zipFilesResult = getZipFilesList(Environment.getExternalStorageDirectory())


        if (zipFilesResult.size > 0) {
            val zipadapter = ZipAdapter(requireContext(), zipFilesResult)
            rvZip.adapter = zipadapter
            rvZip.visibility = View.VISIBLE
            noRecord!!.visibility = View.GONE
            zipadapter.setOnItemClickListener {
                //Handle selected File
            }
        } else {
            //Show No Record
            noRecord!!.visibility = View.VISIBLE
        }
    }
    val storageDocs: ArrayList<File> = ArrayList()
    fun getZipFilesList(dir: File): ArrayList<File> {
        val zipDirFilesList = dir.listFiles()
        if (zipDirFilesList != null) {
            for (i in zipDirFilesList.indices) {
                if (zipDirFilesList[i].isDirectory) {
                    getZipFilesList(zipDirFilesList[i])
                } else {
                    if (zipDirFilesList[i].name.endsWith(".zip")
                    ) {
                        storageDocs.add(zipDirFilesList[i])
                    }
                }
            }
        }
        return storageDocs
    }

}
