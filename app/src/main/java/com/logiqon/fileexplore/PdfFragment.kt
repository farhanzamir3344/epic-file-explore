package com.logiqon.fileexplore

import android.content.Intent.getIntent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ahmer.pdfviewer.PDFView
import java.io.File


class PdfFragment : Fragment() {

    private var filepath: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pdf, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        if (activity?.intent!!.hasExtra("path")) {
            filepath = activity?.intent!!.getStringExtra("path").toString()
            val pdfView = view.findViewById<PDFView>(R.id.pdf_viewer)
            val file = filepath?.let { File(it) }
            val path = Uri.fromFile(file)
            pdfView.fromUri(path).load()
        }
    }

}