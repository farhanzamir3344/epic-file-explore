package com.logiqon.fileexplore;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.C.ContentType;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Tracks;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.RandomTrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionParameters;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import java.util.ArrayList;

/**
 * An activity that plays media using {@link ExoPlayer}.
 */
public class PlayerActivity extends AppCompatActivity
        implements StyledPlayerView.ControllerVisibilityListener {


    // Saved instance state keys.
    private static final String KEY_TRACK_SELECTION_PARAMETERS = "track_selection_parameters";
    private static final String KEY_SERVER_SIDE_ADS_LOADER_STATE = "server_side_ads_loader_state";
    private static final String KEY_ITEM_INDEX = "item_index";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    private StyledPlayerView playerView;
    private ProgressBar progressView;
    private FrameLayout mainMediaFrame;

    private DataSource.Factory dataSourceFactory;
    private ExoPlayer player;
    private DefaultTrackSelector trackSelector;
    private TrackSelectionParameters trackSelectionParameters;
    private Tracks lastSeenTracks;

    private boolean startAutoPlay;
    private int startWindow;
    private int startItemIndex;
    private long startPosition;
    private VideoView playZone;
    private SeekBar seeker;

    private String videoURL, title, imageURL;

    private InterstitialAd mInterstitialAd;
    private Boolean isClose = false;
    // Activity lifecycle

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Window w = getWindow(); // in Activity's onCreate() for instance
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        MobileAds.initialize(this, initializationStatus -> {
        });


       dataSourceFactory = buildDataSourceFactory();
        //    mCastContext = CastContext.getSharedInstance(this);

        setContentView(R.layout.activity_player);


        InterstitialAd.load(this, getString(R.string.fullScreenAdUnit), new AdRequest.Builder().build(), new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                // The mInterstitialAd reference will be null until
                // an ad is loaded.
                mInterstitialAd = interstitialAd;
                Log.i("onAdLoaded", "Ad Loaded");
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                // Handle the error
                Log.i("onAdFailedToLoad", loadAdError.getMessage());
                mInterstitialAd = null;
            }
        });

        if (savedInstanceState != null) {
            trackSelectionParameters =
                    TrackSelectionParameters.fromBundle(
                            savedInstanceState.getBundle(KEY_TRACK_SELECTION_PARAMETERS));
            startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            startItemIndex = savedInstanceState.getInt(KEY_ITEM_INDEX);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
        } else {
            trackSelectionParameters = new TrackSelectionParameters.Builder(/* context= */ this).build();
            clearStartPosition();
        }
    }

    private void handleFullscreenAd() {


        if (mInterstitialAd != null) {

            mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                @Override
                public void onAdDismissedFullScreenContent() {
                    // Called when fullscreen content is dismissed.
                    Log.d("TAG", "The ad was dismissed.");

                    if (isClose)
                        finish();
                }

                @Override
                public void onAdFailedToShowFullScreenContent(AdError adError) {
                    // Called when fullscreen content failed to show.
                    Log.d("TAG", "The ad failed to show.");

                    if (isClose)
                        finish();
                }

                @Override
                public void onAdShowedFullScreenContent() {
                    // Called when fullscreen content is shown.
                    // Make sure to set your reference to null so you don't
                    // show it a second time.
                    mInterstitialAd = null;
                    Log.d("TAG", "The ad was shown.");
                }
            });
        }


    }




    private void initGUI() {
        mainMediaFrame = findViewById(R.id.mainMediaFrame);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();


        if (Util.SDK_INT <= 23 || player == null) {

            videoURL = getIntent().getStringExtra(Constant.CONTENT_URL);
            title = getIntent().getStringExtra(Constant.CONTENT_NAME);
            imageURL = getIntent().getStringExtra(Constant.CONTENT_IMG_URL);

            initGUI();

            setCurrentUrl(videoURL);

            if (playerView != null) {
                playerView.onResume();
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            if (playerView != null) {
                playerView.onPause();
            }
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            if (playerView != null) {
                playerView.onPause();
            }
            releasePlayer();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        updateTrackSelectorParameters();
        updateStartPosition();
        outState.putBundle(KEY_TRACK_SELECTION_PARAMETERS, trackSelectionParameters.toBundle());
        outState.putBoolean(KEY_AUTO_PLAY, startAutoPlay);
        outState.putInt(KEY_ITEM_INDEX, startItemIndex);
        outState.putLong(KEY_POSITION, startPosition);
    }

    // Activity input

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // See whether the player view wants to handle media or DPAD keys events.
        return playerView.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }


    // PlaybackControlView.PlaybackPreparer implementation
// OnClickListener methods


    // Internal methods

    @SuppressLint("CheckResult")
    public void setCurrentUrl(String url) {

        if (playerView == null) {

            playerView = findViewById(R.id.player_view);

            progressView = findViewById(R.id.progressView);
            playerView.setControllerVisibilityListener(this);
            playerView.setErrorMessageProvider(new PlayerErrorMessageProvider());
            playerView.requestFocus();


            initButton();

        }

        if (url != null && !url.isEmpty())
            initializePlayer(url);

        //handleVideoCast();

    }

    private void initializePlayer(String url) {

        if (player == null) {

            RandomTrackSelection.Factory trackSelectionFactory = new RandomTrackSelection.Factory();
            LoadControl loadControl = new DefaultLoadControl();


            trackSelector = new DefaultTrackSelector(this);
            trackSelector.setParameters(trackSelectionParameters);
            lastSeenTracks = null;

            ExoPlayer.Builder playerBuilder =
                    new ExoPlayer.Builder(/* context= */ this)
                            .setMediaSourceFactory(createMediaSourceFactory(Uri.parse(url)));

            playerBuilder.setRenderersFactory(new DefaultRenderersFactory(this));

            player = playerBuilder.build();
            player.setTrackSelectionParameters(trackSelectionParameters);
            player.addListener(new PlayerEventListener());
            player.setPlayWhenReady(startAutoPlay);

            playerView.setPlayer(player);

        }

        boolean haveStartPosition = startWindow != C.INDEX_UNSET;
        if (haveStartPosition) {
            player.seekTo(startWindow, startPosition);
        }
        MediaItem mediaItem = new MediaItem.Builder()
                .setUri(Uri.parse(url))
                .build();

        player.setMediaItem(mediaItem, /* resetPosition= */ !haveStartPosition);
        player.prepare();

//        player.prepare(mediaSource, !haveStartPosition, false);
    }

    private MediaSource.Factory createMediaSourceFactory(Uri uri) {
        @ContentType int type = Util.inferContentType(uri);
        switch (type) {
            case C.CONTENT_TYPE_DASH:
                return new DashMediaSource.Factory(dataSourceFactory);
            case C.CONTENT_TYPE_SS:
                return new SsMediaSource.Factory(dataSourceFactory);
            case C.CONTENT_TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory);
            case C.CONTENT_TYPE_OTHER:
                return new ProgressiveMediaSource.Factory(dataSourceFactory);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }


    private void releasePlayer() {
        if (player != null) {
            updateTrackSelectorParameters();
            updateStartPosition();
            player.release();
            player = null;
            trackSelector = null;
        }


    }


    private void updateTrackSelectorParameters() {
        if (player != null) {
            trackSelectionParameters = player.getTrackSelectionParameters();
        }
    }

    private void updateStartPosition() {
        if (player != null) {
            startAutoPlay = player.getPlayWhenReady();
            startWindow = player.getCurrentMediaItemIndex();
            startPosition = Math.max(0, player.getContentPosition());
        }
    }

    private void clearStartPosition() {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    /**
     * Returns a new DataSource factory.
     */
    private DataSource.Factory buildDataSourceFactory() {
        return ((EpicFileExplorerApplication) getApplication()).buildDataSourceFactory();
    }


    //region User controls

    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private static boolean isBehindLiveWindow(PlaybackException e) {
        if (e.errorCode != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getCause();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    //endregion

    @Override
    public void onVisibilityChanged(int visibility) {

    }

    private class PlayerEventListener implements Player.Listener {

        @Override
        public void onPlaybackStateChanged(int playbackState) {

            if (playbackState == Player.STATE_READY) {
                // media actually playing
                if (progressView != null)
                    progressView.setVisibility(View.GONE);

            } else if (playbackState == Player.STATE_BUFFERING) {
                if (progressView != null)
                    progressView.setVisibility(View.VISIBLE);
            } else if (playbackState == Player.STATE_ENDED) {

                if (mInterstitialAd != null) {
                    isClose = false;
                    handleFullscreenAd();
                    mInterstitialAd.show(PlayerActivity.this);
                }

            } else {
                // player paused in any state

                if (progressView != null)
                    progressView.setVisibility(View.GONE);
            }

        }

        @Override
        public void onPlayerError(PlaybackException error) {
            if (isBehindLiveWindow(error)) {
                clearStartPosition();
                setCurrentUrl(videoURL);
            }

        }

        @Override
        public void onTracksChanged(Tracks tracks) {

            if (tracks == lastSeenTracks) {
                return;
            }
            if (tracks.containsType(C.TRACK_TYPE_VIDEO)
                    && !tracks.isTypeSupported(C.TRACK_TYPE_VIDEO, /* allowExceedsCapabilities= */ true)) {
                showToast(R.string.error_unsupported_video);
            }
            if (tracks.containsType(C.TRACK_TYPE_AUDIO)
                    && !tracks.isTypeSupported(C.TRACK_TYPE_AUDIO, /* allowExceedsCapabilities= */ true)) {
                showToast(R.string.error_unsupported_audio);
            }
            lastSeenTracks = tracks;
        }

    }

    private class PlayerErrorMessageProvider implements ErrorMessageProvider<PlaybackException> {

        @Override
        public Pair<Integer, String> getErrorMessage(PlaybackException e) {
            String errorString = getString(R.string.error_generic);
            Throwable cause = e.getCause();
            if (cause instanceof DecoderInitializationException) {
                // Special case for decoder initialization failures.
                DecoderInitializationException decoderInitializationException =
                        (DecoderInitializationException) cause;
                if (decoderInitializationException.codecInfo == null) {
                    if (decoderInitializationException.getCause() instanceof DecoderQueryException) {
                        errorString = getString(R.string.error_querying_decoders);
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        errorString =
                                getString(
                                        R.string.error_no_secure_decoder, decoderInitializationException.mimeType);
                    } else {
                        errorString =
                                getString(R.string.error_no_decoder, decoderInitializationException.mimeType);
                    }
                } else {
                    errorString =
                            getString(
                                    R.string.error_instantiating_decoder,
                                    decoderInitializationException.codecInfo.name);
                }
            }
            return Pair.create(0, errorString);
        }
    }

    //region fullscreen handling


    private void initButton() {

        ImageView ivRewind = findViewById(R.id.exo_rewind);
        ImageView ivForward = findViewById(R.id.exo_forward);
        ImageView finish = findViewById(R.id.finishScreen);
        TextView contentName = findViewById(R.id.tvContentName);

        //  mMediaRouteButton = findViewById(R.id.mediaRouteCastButton);;
        //    mExternalRouteEnabledDrawable = ColorCastUtil.getMediaRouteButtonDrawable(this);
        //    DrawableCompat.setTint(mExternalRouteEnabledDrawable, getResources().getColor(R.color.white));
        //     mMediaRouteButton.setRemoteIndicatorDrawable(mExternalRouteEnabledDrawable);

        contentName.setText(title);

        finish.setOnClickListener(view -> {
            if (mInterstitialAd != null) {
                isClose = true;
                handleFullscreenAd();
                mInterstitialAd.show(PlayerActivity.this);
            } else {
                onBackPressed();
            }

        });

        ivRewind.setOnClickListener(view -> {

            if (player.getCurrentPosition() > 10000)
                player.seekTo(player.getCurrentPosition() - 10000);
        });


        ivForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.seekTo(player.getCurrentPosition() + 10000);
            }
        });
    }
    //endregion

}
