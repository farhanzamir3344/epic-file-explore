package com.logiqon.fileexplore;

import java.io.Serializable;

public class Constant implements Serializable {

    public static final String CONTENT_URL = "contentURL";
    public static final String CONTENT_IMG_URL = "contentImageURL";
    public static final String CONTENT_NAME = "contentName";
}
