package com.logiqon.fileexplore

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.gms.ads.*
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var navcontroller: NavController
    lateinit var mAdView: AdView

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        handleGoogleBannerAd()
        toolBarNavigateUp()
    }
    private fun toolBarNavigateUp() {

        // val actionBar: ActionBar? = supportActionBar
        // actionBar!!.setDisplayShowTitleEnabled(false)


        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.homeToolbar)
        setSupportActionBar(toolbar)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.flFragmentContainer) as NavHostFragment
        navcontroller = navHostFragment.navController
        supportActionBar!!.hide()

        setupActionBarWithNavController(navcontroller)


        // toolbar.setupWithNavController(navcontroller)


        navHostFragment.childFragmentManager.addOnBackStackChangedListener {


            if (navHostFragment.childFragmentManager.backStackEntryCount == 0) {

                supportActionBar!!.hide()
            } else {
                supportActionBar!!.show()
            }
        }
//        window.setDecorFitsSystemWindows(false)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    }

    private fun handleGoogleBannerAd() {
        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        kotlin.run { }

        val tt: TimerTask = object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    mAdView.loadAd(adRequest)
                }
            }
        }

        val t = Timer()
        t.scheduleAtFixedRate(tt, 0, 600 * 60)

//        mAdView.visibility = GONE

        mAdView.adListener = object : AdListener() {
            override fun onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            override fun onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
                super.onAdLoaded()
                Toast.makeText(this@MainActivity, "Add Loaded!", Toast.LENGTH_SHORT).show()
            }

            override fun onAdFailedToLoad(adError: LoadAdError) {
                // Code to be executed when an ad request fails.
                super.onAdFailedToLoad(adError)
                mAdView.loadAd(adRequest)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navcontroller.navigateUp() || super.onSupportNavigateUp()
    }
}