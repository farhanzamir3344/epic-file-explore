package com.logiqon.fileexplore.utils

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.BitmapFactory
import android.os.Build
import android.provider.MediaStore
import android.provider.MediaStore.Audio.AlbumColumns.ALBUM_ART
import android.util.Log
import android.util.Size
import androidx.core.database.getLongOrNull
import androidx.core.database.getStringOrNull
import com.logiqon.fileexplore.Model.ImagesDTO
import com.logiqon.fileexplore.Model.MusicDTO
import com.logiqon.fileexplore.Model.VideoDTO
import java.io.File
import java.io.FileNotFoundException

object FileUtils {

    val File.size get() = if (!exists()) 0.0 else length().toDouble()
    val File.sizeInKb get() = size / 1024
    val File.sizeInMb get() = sizeInKb / 1024
    val File.sizeInGb get() = sizeInMb / 1024
    val File.sizeInTb get() = sizeInGb / 1024

    fun File.sizeStr(): String = size.toString()
    fun File.sizeStrInKb(decimals: Int = 0): String = "%.${decimals}f".format(sizeInKb)
    fun File.sizeStrInMb(decimals: Int = 0): String = "%.${decimals}f".format(sizeInMb)
    fun File.sizeStrInGb(decimals: Int = 0): String = "%.${decimals}f".format(sizeInGb)

    fun File.sizeStrWithBytes(): String = sizeStr() + "b"
    fun File.sizeStrWithKb(decimals: Int = 0): String = sizeStrInKb(decimals) + "KB"
    fun File.sizeStrWithMb(decimals: Int = 0): String = sizeStrInMb(decimals) + "MB"
    fun File.sizeStrWithGb(decimals: Int = 0): String = sizeStrInGb(decimals) + "GB"



    //All Videos from External URI
    fun getAllVideosFromExternalStorage(mContext : Context): ArrayList<VideoDTO> {
        val storageDocs: ArrayList<VideoDTO> = ArrayList()

        val videoProjection = arrayOf(
            MediaStore.Video.Media.DISPLAY_NAME,
            MediaStore.Video.Media.SIZE,
            MediaStore.Video.Media.DATE_TAKEN,
            MediaStore.Video.Media._ID,
            MediaStore.Video.Media.DATA
        )
        val sortOrder = "${MediaStore.Video.Media.DATE_TAKEN} DESC"
        val cursor = mContext.contentResolver.query(
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
            videoProjection,
            null,
            null,
            sortOrder
        )
        cursor.use {
            it?.let {
                val idColumn = it.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
                val nameColumn = it.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME)
                val sizeColumn = it.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE)
                val dateColumn = it.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_TAKEN)
                val data = it.getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
                while (it.moveToNext()) {
                    val id = it.getLong(idColumn)
                    val name = it.getString(nameColumn)
                    val size = it.getString(sizeColumn)
                    val date = it.getString(dateColumn)
                    val filePath = it.getString(data)
                    val contentUri = ContentUris.withAppendedId(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        id
                    )
                    // add the URI to the list
                    // generate the thumbnail
                    val thumbnail = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        mContext.contentResolver.loadThumbnail(contentUri,
                            Size(480, 480), null)
                    } else {
                        MediaStore.Images.Thumbnails.getThumbnail(mContext.contentResolver, id, MediaStore.Video.Thumbnails.MINI_KIND, null)
                    }

                    val videoDTO = VideoDTO(id, name, size, date, contentUri, filePath, thumbnail)
                    storageDocs.add(videoDTO)

                }
            } ?: kotlin.run {
                Log.e("TAG", "Cursor is null!")
            }
        }

        return storageDocs
    }

    //All Images from External URI
    fun getAllImagesFromExternalStorage(mContext : Context): ArrayList<ImagesDTO> {
        val imagesList: ArrayList<ImagesDTO> = ArrayList()

        val imgProjection = arrayOf(
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE,
            MediaStore.Images.Media.DATE_TAKEN,
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DATA
        )
        val sortOrder = "${MediaStore.Images.Media.DATE_TAKEN} DESC"
        val cursor = mContext.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            imgProjection,
            null,
            null,
            sortOrder
        )
        cursor.use {
            it?.let {
                val idColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
                val nameColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
                val sizeColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE)
                val dateColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_TAKEN)
                val data = it.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                while (it.moveToNext()) {
                    val id = it.getLong(idColumn)
                    val name = it.getString(nameColumn)
                    val size = it.getString(sizeColumn)
                    val date = it.getString(dateColumn)
                    val filePath = it.getString(data)
                    val contentUri = ContentUris.withAppendedId(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        id
                    )
                    // add the URI to the list
                    // generate the thumbnail
                    val thumbnail = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        try{
                            mContext.contentResolver.loadThumbnail(contentUri,
                                Size(480, 480), null)
                        }catch (ex: FileNotFoundException){
                            ex.printStackTrace()
                            null
                        }catch (ex: Exception){
                            ex.printStackTrace()
                            null
                        }

                    } else {
                        MediaStore.Images.Thumbnails.getThumbnail(mContext.contentResolver, id, MediaStore.Images.Thumbnails.MINI_KIND, null)
                    }

                    val imagesDTO = ImagesDTO(id, name, size, date, contentUri, filePath, thumbnail)
                    imagesList.add(imagesDTO)

                }
            } ?: kotlin.run {
                Log.e("TAG", "Cursor is null!")
            }
        }

        return imagesList
    }


    //All Audio from External URI
    fun getAllAudioFromExternalStorage(mContext : Context): ArrayList<MusicDTO> {
        val audioList: ArrayList<MusicDTO> = ArrayList()

        val audioProjection = arrayOf(
            MediaStore.Audio.Media.DISPLAY_NAME,
            MediaStore.Audio.Media.SIZE,
            MediaStore.Audio.Media.DATE_ADDED,
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.DATA

        )
        val albumProjection = arrayOf(
            MediaStore.Audio.Albums.ALBUM_ART,
            MediaStore.Audio.Albums.ALBUM_ID,
            MediaStore.Audio.Albums._ID
        )

        val sortOrder = "${MediaStore.Audio.Media.DATE_ADDED} DESC"
        val cursor = mContext.contentResolver.query(
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
            audioProjection,
            null,
            null,
            sortOrder
        )
        cursor.use {
            it?.let {
                val idColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)
                val nameColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME)
                val sizeColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.SIZE)
                val dateColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.DATE_ADDED)
                val durationColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)
                val artistColumn = it.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST)
                val data = it.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
                while (it.moveToNext()) {
                    val id = it.getLongOrNull(idColumn)
                    val name = it.getStringOrNull(nameColumn)
                    val size = it.getStringOrNull(sizeColumn)
                    val date = it.getStringOrNull(dateColumn)
                    val filePath = it.getStringOrNull(data)
                    val duration = it.getStringOrNull(durationColumn)
                    val artist = it.getStringOrNull(artistColumn)
                    val contentUri = ContentUris.withAppendedId(
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        id!!
                    )
                    // add the URI to the list
                    // generate the thumbnail
                    val thumbnail = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        try{
                            mContext.contentResolver.loadThumbnail(contentUri,
                                Size(480, 480), null)
                        }catch (ex: FileNotFoundException){
                            ex.printStackTrace()
                            null
                        }catch (ex: Exception){
                            ex.printStackTrace()
                            null
                        }

                    } else {
                        val cur: Cursor? = mContext.contentResolver.query(
                            MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                            albumProjection,
                            null,
                            null,
                            null
                        )

                        if (cur != null) {
                            if (cur.moveToFirst()) {
                                val path: String = cur.getString(cur.getColumnIndexOrThrow(ALBUM_ART))
                                // do whatever you need to do
                                BitmapFactory.decodeFile(path)
                            }else
                                null
                        }else
                            null

                    }

                    val musicDTO = MusicDTO(id, name, size, date, contentUri, filePath, thumbnail, duration, artist)
                    audioList.add(musicDTO)

                }
            } ?: kotlin.run {
                Log.e("TAG", "Cursor is null!")
            }
        }

        return audioList
    }
}