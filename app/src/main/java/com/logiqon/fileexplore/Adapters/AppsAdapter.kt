package com.logiqon.fileexplore.Adapters

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.logiqon.fileexplore.R
import com.logiqon.fileexplore.utils.FileUtils.sizeStrWithMb
import java.io.File


class AppsAdapter(
    var context1: Context, var apkfiles: ArrayList<File>,
) :
    RecyclerView.Adapter<AppsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context1)
            .inflate(R.layout.appcardview, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return apkfiles.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageview: ImageView = itemView.findViewById(R.id.ivAPK)
        val tvAppName: TextView = itemView.findViewById(R.id.tvName)
        val tvAppSize: TextView = itemView.findViewById(R.id.tvFileSize)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val file = apkfiles[position]


        try{
            val packageInfo: PackageInfo? = context1.getPackageManager()
                .getPackageArchiveInfo(file.path, PackageManager.GET_ACTIVITIES)
            if (packageInfo != null) {
                val appInfo = packageInfo.applicationInfo
                if (Build.VERSION.SDK_INT >= 8) {
                    appInfo.sourceDir = file.path
                    appInfo.publicSourceDir = file.path
                }
                val icon = appInfo.loadIcon(context1.getPackageManager())

                Glide.with(context1)
                    .load(icon).apply(RequestOptions().placeholder(R.drawable.ivapk))
                    .into(holder.imageview)
            }
        }catch (ex: Exception){
            ex.printStackTrace()
        }


        holder.tvAppName.text = file.name
        holder.tvAppSize.text = file.sizeStrWithMb(2)

        holder.itemView.setOnClickListener {
            onItemClickListener?.let {
                it(file)
            }
        }
    }

    private var onItemClickListener: ((File) -> Unit)? = null

    fun setOnItemClickListener(listener: (File) -> Unit) {
        onItemClickListener = listener
    }
}