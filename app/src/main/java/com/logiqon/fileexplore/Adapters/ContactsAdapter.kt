package com.logiqon.fileexplore.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.logiqon.fileexplore.R
import java.io.File


class ContactsAdapter(var context1: Context, var vcffiles: ArrayList<File>) :
    RecyclerView.Adapter<ContactsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context1)
            .inflate(R.layout.contactscardview, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return vcffiles.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val tvVCF: TextView = itemView.findViewById(R.id.tvVcfFileName)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val file = vcffiles[position]

        holder.tvVCF.text = file.name

        holder.itemView.setOnClickListener {
            onItemClickListener?.let {
                it(file)
            }
        }
    }
    private var onItemClickListener: ((File) -> Unit)? = null

    fun setOnItemClickListener(listener: (File) -> Unit) {
        onItemClickListener = listener
    }
}