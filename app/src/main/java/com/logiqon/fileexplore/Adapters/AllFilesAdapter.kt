package com.logiqon.fileexplore.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.logiqon.fileexplore.R
import com.logiqon.fileexplore.fragment.AppsFragment
import java.io.File


class AllFilesAdapter(
    var context1: Context, var apkfiles: ArrayList<File>,
) :
    RecyclerView.Adapter<AllFilesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context1)
            .inflate(R.layout.allfilescardview, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return apkfiles.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardView: CardView = itemView.findViewById(R.id.card_view_allfiles)
        val imageview: ImageView = itemView.findViewById(R.id.imageviewallfiles)
        val textView: TextView = itemView.findViewById(R.id.Apk_Nameallfiles)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val file = apkfiles[position]
        Glide.with(context1)
            .load(file).apply(RequestOptions().placeholder(R.drawable.zip2).centerCrop())
            .into(holder.imageview)
        holder.textView.text = file.name
        holder.cardView.setOnClickListener {
            onItemClickListener?.let {
                it(file)
            }
        }
    }

    private var onItemClickListener: ((File) -> Unit)? = null

    fun setOnItemClickListener(listener: (File) -> Unit) {
        onItemClickListener = listener
    }
}