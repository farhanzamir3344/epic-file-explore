package com.logiqon.fileexplore.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.logiqon.fileexplore.Model.VideoDTO
import com.logiqon.fileexplore.R
import java.io.File


class VideosAdapter(var context1: Context, var imagefiles: ArrayList<VideoDTO>) :
    RecyclerView.Adapter<VideosAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context1)
            .inflate(R.layout.videoscardview, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return imagefiles.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val cardView: CardView = itemView.findViewById(R.id.videoCard)
        val imageview: ImageView = itemView.findViewById(R.id.videoPreview)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val file = imagefiles[position]
        Glide.with(context1)
            .load(file.thumbnail).apply(RequestOptions().placeholder(R.drawable.placeholder).centerCrop())
            .into(holder.imageview)

        holder.cardView.setOnClickListener {
            onItemClickListener?.let {
                it(file)
            }
        }

    }

    private var onItemClickListener: ((VideoDTO) -> Unit)? = null

    fun setOnItemClickListener(listener: (VideoDTO) -> Unit) {
        onItemClickListener = listener

    }
}