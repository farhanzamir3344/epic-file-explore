package com.logiqon.fileexplore.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.logiqon.fileexplore.R
import com.logiqon.fileexplore.onPdfSelectListener
import java.io.File


class DocumentsAdapter(var context1: Context,
                       var docfiles: ArrayList<File>)
    : RecyclerView.Adapter<DocumentsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context1)
            .inflate(R.layout.documentscardview, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val file = docfiles[position]
        if (file.name.endsWith("pdf")) {
            Glide.with(context1)
                .load(file).apply(RequestOptions().placeholder(R.drawable.pdf1111).centerCrop())
                .into(holder.imageview)
        }else{
            Glide.with(context1)
                .load(file).apply(RequestOptions().placeholder(R.drawable.wordlogo).centerCrop())
                .into(holder.imageview)
        }
        holder.textView.text = file.name

        holder.cardView.setOnClickListener {
            onItemClickListener?.let {
                it(file)
            }
        }

    }

    private var onItemClickListener: ((File) -> Unit)? = null

    fun setOnItemClickListener(listener: (File) -> Unit) {
        onItemClickListener = listener
    }
    override fun getItemCount(): Int {
        return docfiles.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val cardView: CardView = itemView.findViewById(R.id.card_view33)
        val imageview: ImageView = itemView.findViewById(R.id.imageview)
        val textView: TextView = itemView.findViewById(R.id.Apk_Name)


    }

}