package com.logiqon.fileexplore.Adapters

import android.content.Context
import android.content.Intent
import android.icu.text.Transliterator
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.logiqon.fileexplore.Model.MusicDTO
import com.logiqon.fileexplore.R
import com.logiqon.fileexplore.fragment.AppsFragment
import java.io.File


class MusicAdapter(
    var context1: Context, var audioFiles: ArrayList<MusicDTO>,
) :
    RecyclerView.Adapter<MusicAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context1)
            .inflate(R.layout.audio_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return audioFiles.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardView: CardView = itemView.findViewById(R.id.card_view_audio)
        val ivAudio: ImageView = itemView.findViewById(R.id.ivAudio)
        val audioName: TextView = itemView.findViewById(R.id.tvAudioName)
        val audioArtist: TextView = itemView.findViewById(R.id.tvArtistName)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val file = audioFiles[position]
        Glide.with(context1)
            .load(file.thumbnail)
            .apply(RequestOptions().placeholder(R.drawable.ic_baseline_music_note_24).centerCrop())
            .into(holder.ivAudio)

        holder.audioName.text = file.name
        holder.audioArtist.text = file.artistName

        holder.cardView.setOnClickListener {
            onItemClickListener?.let {
                it(file)
            }
        }
    }

    private var onItemClickListener: ((MusicDTO) -> Unit)? = null



    fun setOnItemClickListener(listener: (MusicDTO) -> Unit) {
        onItemClickListener = listener

    }

}