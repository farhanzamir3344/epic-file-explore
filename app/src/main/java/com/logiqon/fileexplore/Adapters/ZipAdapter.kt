package com.logiqon.fileexplore.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.logiqon.fileexplore.R
import java.io.File


class ZipAdapter(var context1: Context, var zipfiles: ArrayList<File>) :
    RecyclerView.Adapter<ZipAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context1)
            .inflate(R.layout.zipcardview, parent, false)

        return ViewHolder(view)
    }

    fun getListCount() : Int{
        return zipfiles.size
    }

    override fun getItemCount(): Int {
        return zipfiles.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val tvZipName: TextView = itemView.findViewById(R.id.tvZipName)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val file = zipfiles[position]
        holder.tvZipName.text = file.name

        holder.itemView.setOnClickListener {
            onItemClickListener?.let {
                it(file)
            }
        }

    }

    private var onItemClickListener: ((File) -> Unit)? = null

    fun setOnItemClickListener(listener: (File) -> Unit) {
        onItemClickListener = listener

    }
}