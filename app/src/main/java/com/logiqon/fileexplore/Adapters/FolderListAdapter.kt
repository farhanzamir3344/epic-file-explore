package com.logiqon.fileexplore.Adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.logiqon.fileexplore.Model.FolderDTO
import com.logiqon.fileexplore.R


class FolderListAdapter(
    private var mList: ArrayList<FolderDTO>,
    private val listener: onItemClickListener,
) :
    RecyclerView.Adapter<FolderListAdapter.ViewHolder>(),Filterable {

    var filterResults = ArrayList<FolderDTO>()
    init {
        filterResults = mList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cardview, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val folderItem = filterResults[position]

        // sets the image to the imageview from our itemHolder class
        holder.imageView.setImageResource(folderItem.image)

        // sets the text to the textview from our itemHolder class
        holder.textView.text = folderItem.text

    }
    override fun getItemCount(): Int {
        return filterResults.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener{
        val imageView: ImageView = itemView.findViewById(R.id.imageview)
        val textView: TextView = itemView.findViewById(R.id.textviewname)
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }
    interface onItemClickListener{
        fun onItemClick(position: Int)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    filterResults = mList
                } else {
                    val resultList = ArrayList<FolderDTO>()
                    for (row in mList) {
                        if (row.text.lowercase().contains(charSearch.lowercase())) {
                            resultList.add(row)
                        }
                    }
                    filterResults = resultList
                }
                val fResults = FilterResults()
                fResults.values = filterResults
                return fResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filterResults = results?.values as ArrayList<FolderDTO>
                notifyDataSetChanged()
            }
        }
    }
}