package com.logiqon.fileexplore.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.logiqon.fileexplore.Model.ImagesDTO
import com.logiqon.fileexplore.R
import java.io.File


class ImagesAdapter(var context1: Context, var imagefiles: ArrayList<ImagesDTO>) :
    RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context1)
            .inflate(R.layout.imagescardview, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return imagefiles.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val imageview: ImageView = itemView.findViewById(R.id.picture)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val file = imagefiles[position]
        Glide.with(context1)
            .load(file.thumbnail).apply(RequestOptions().placeholder(R.drawable.placeholder).centerCrop())
            .into(holder.imageview)

        holder.itemView.setOnClickListener {
            onItemClickListener?.let {
                it(file)
            }
        }

    }

    private var onItemClickListener: ((ImagesDTO) -> Unit)? = null

    fun setOnItemClickListener(listener: (ImagesDTO) -> Unit) {
        onItemClickListener = listener

    }
}