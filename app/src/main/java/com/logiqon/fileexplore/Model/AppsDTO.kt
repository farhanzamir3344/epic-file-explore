package com.logiqon.fileexplore.Model

import android.content.pm.ApplicationInfo

/**
 * Created by rajdeep1008 on 19/04/18.
 */

data class AppsDTO(val appInfo: ApplicationInfo,
               val appName: String,
               val packageName: String? = "",
               val version: String? = "")