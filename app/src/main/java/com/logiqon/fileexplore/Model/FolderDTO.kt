package com.logiqon.fileexplore.Model

data class FolderDTO(
    val image: Int, val text: String) {
}