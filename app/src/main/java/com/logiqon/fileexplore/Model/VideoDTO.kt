package com.logiqon.fileexplore.Model

import android.graphics.Bitmap
import android.net.Uri

data class VideoDTO(
    val id: Long,
    val name: String,
    var size:String,
    var date:String,
    var contentUri: Uri,
    var filePath: String?,
    var thumbnail: Bitmap
)
