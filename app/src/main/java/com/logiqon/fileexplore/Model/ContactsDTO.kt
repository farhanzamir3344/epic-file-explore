package com.logiqon.fileexplore.Model

class ContactVO {
    var contactImage: String? = null
        set(contactImage) {
            field = this.contactImage
        }
    var contactName: String? = null
    var contactNumber: String? = null
}