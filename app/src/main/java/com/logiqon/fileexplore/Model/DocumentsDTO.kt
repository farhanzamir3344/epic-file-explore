package com.logiqon.fileexplore.Model

import java.io.File

data class DocumentsDTO(
    val image: ArrayList<File>, val text: String) {
}