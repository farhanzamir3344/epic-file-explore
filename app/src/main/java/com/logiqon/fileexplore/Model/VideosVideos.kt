package com.logiqon.fileexplore.Model

class Model_Video {

    var str_thumb:String?=null
    var str_path: String? = null
    var boolean_selected = false

    @JvmName("getStr_path1")
    fun getStr_path(): String? {
        return str_path
    }

    @JvmName("setStr_path1")
    fun setStr_path(str_path: String) {
        this.str_path = str_path
    }
    @JvmName("getStr_thumb1")
    fun getStr_thumb(): String? {
        return str_thumb
    }
    @JvmName("setStr_thumb1")
    fun setStr_thumb(str_thumb: String?) {
        this.str_thumb = str_thumb
    }

    fun isBoolean_selected(): Boolean {
        return boolean_selected
    }

    @JvmName("setBoolean_selected1")
    fun setBoolean_selected(boolean_selected: Boolean) {
        this.boolean_selected = boolean_selected
    }
}